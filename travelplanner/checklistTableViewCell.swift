//
//  checklistTableViewCell.swift
//  travelplanner
//
//  Created by James Costello on 30/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit

class checklistTableViewCell: UITableViewCell {
    
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var isDoneImge: UIImageView!
    @IBOutlet weak var qtyButton: UIButton!
    @IBOutlet weak var taskId: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
