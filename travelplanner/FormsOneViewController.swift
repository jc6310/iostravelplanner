//
//  FormsOneViewController.swift
//  travelplanner
//
//  Created by James Costello on 26/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit
import CoreData

class FormsOneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var typeReceived = ""
    var detailReceived = [Myplans]()
    var tripDetail = [Mytrips]()
    var ranking : [Double] = []
    var trip_id = ""
    var plans_id = ""
    let datePickerView:UIDatePicker = UIDatePicker()
    var isEditReceived = ""
    var MyChecklists = [Planstypechecklist]()
    let cellReuseIdentifier = "cell"
    var moc:NSManagedObjectContext!
    
    @IBOutlet weak var addItemsButton: UIBarButtonItem!
    @IBOutlet weak var addNewChecklistButton: UIBarButtonItem!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var datePickerTextfield: UITextField!
    
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        moc = appDel.persistentContainer.viewContext
        
        datePickerTextfield.delegate = self
        self.datePickerTextfield.inputView = self.datePickerView
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        self.datePickerTextfield.inputAccessoryView = self.createToolbar()
        tableview.dataSource = self
        tableview.delegate = self
        
        if(typeReceived=="Checklist"){
            title = typeReceived
            
            addNewChecklistButton.isEnabled = true
            addNewChecklistButton.image = UIImage(named: "icons8-save-all-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            addItemsButton.isEnabled = false
            addItemsButton.image = nil

        }else{
            title = (detailReceived.first?.value(forKeyPath: "destination") as? String)!
            plans_id = (detailReceived.first?.value(forKeyPath: "plans_id") as? String)!
            
            addNewChecklistButton.isEnabled = false
            addNewChecklistButton.image = nil
            
            addItemsButton.isEnabled = true
            addItemsButton.image = UIImage(named: "icons8-plus-48.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)

            if(isEditReceived == "view"){
                locationTextField.isEnabled = false
                datePickerTextfield.isEnabled = false
            }

            locationTextField.text =  detailReceived.first?.value(forKeyPath: "destination") as? String
            datePickerTextfield.text =  detailReceived.first?.value(forKeyPath: "date") as? String
            
            locationTextField.isEnabled = false
            datePickerTextfield.isEnabled = false
            
            loadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#0A0D20")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        
        addItemsButton.width = CGFloat(32)
        addNewChecklistButton.width = CGFloat(32)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func loadData(){

        let MyChecklistRequest:NSFetchRequest<Planstypechecklist> = Planstypechecklist.fetchRequest()

        let sortDescriptor = NSSortDescriptor(key: "isdone", ascending: true)
        MyChecklistRequest.sortDescriptors = [sortDescriptor]
        MyChecklistRequest.predicate = NSPredicate(format:"%K == %@", "plans_id", plans_id)
        

        do {
            try MyChecklists = moc.fetch(MyChecklistRequest)
        } catch {
            print("Could not load data")
        }

        self.tableview.reloadData()
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel",
                                                                           comment: ""),
                                                  style: .plain,
                                                  target: self, action: #selector(hideDatePicker))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil, action: nil)
        let doneBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done",
                                                                         comment: ""),
                                                style: .done,
                                                target: self, action: #selector(doneDatePicker))
        toolbar.setItems([cancelBarButtonItem, flexBarButtonItem, doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    @objc func hideDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc func doneDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let selectedDate = self.datePickerView.date
        ranking.insert(selectedDate.timeIntervalSince1970, at: 0)
        self.datePickerTextfield.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return MyChecklists.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        height = 50
        
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let listMyTrips = MyChecklists[indexPath.row]
        
        let cell:checklistTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! checklistTableViewCell
    
        cell.taskId?.text =
            listMyTrips.value(forKeyPath: "item_id") as? String
        cell.taskLabel?.text =
            listMyTrips.value(forKeyPath: "task") as? String
        
        let qty:String = (listMyTrips.value(forKeyPath: "qty") as? String)!
        
        cell.taskId.isHidden = true
        
        cell.layer.borderWidth = CGFloat(3)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        
        cell.qtyButton.setTitle(qty, for: [])
        cell.qtyButton.tag = indexPath.row
        
        cell.qtyButton.addTarget(self, action: #selector(handleButtonTapped(sender:)), for: .touchUpInside)
        
        let isDone = listMyTrips.value(forKeyPath: "isdone") as? Bool
        
        if(isDone==false){
            cell.isDoneImge.image = UIImage(named:"icons8-round-filled-50.png")
        }else{
            cell.isDoneImge.image = UIImage(named:"icons8-checkmark-filled-46.png")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
       let cell:checklistTableViewCell = tableView.cellForRow(at: indexPath as IndexPath) as! checklistTableViewCell
       print(MyChecklists[indexPath.row])
         if(self.MyChecklists[indexPath.row].isdone==false){
        
          cell.isDoneImge.image = UIImage(named:"icons8-round-filled-50.png")
            updateItemIdDone(task: MyChecklists[indexPath.row], isDone: true)
         }else{
        
         cell.isDoneImge.image = UIImage(named:"icons8-checkmark-filled-46.png")
            updateItemIdDone(task: MyChecklists[indexPath.row], isDone: false)
         }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "checklistToPlans" {
            let viewController = segue.destination as! SecondViewController
               viewController.detailReceived = self.tripDetail
        }
    }

    @IBAction func addNewItemsToList(_ sender: Any) {

        let alert = UIAlertController(
            title: NSLocalizedString("New item",
                                     comment: "")  ,
            message:  NSLocalizedString( "",
                                         comment: ""),
            preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: nil)
       
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",
                                                               comment: ""),
                                      style: .cancel,
                                      handler: nil))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK",
                                                               comment: ""),
                                      style: .default,
                                      handler: { (_) in
           if let title = alert.textFields?[0].text
            {
                self.addNewToDoItem(title: title )
            }
            
        }))

        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func handleButtonTapped(sender: UIButton) {
        let selectedIndex = IndexPath(row: sender.tag, section: 0)
    
           tableview.selectRow(at: selectedIndex, animated: true, scrollPosition: .none)
        
        let selectedCell = tableview.cellForRow(at: selectedIndex) as! checklistTableViewCell

        let tempTaskId = "\(selectedCell.taskId.text  ?? "")"
       
        self.updateAlert(tempTaskId: tempTaskId)
    }
    
    func updateAlert(tempTaskId: String ){
      
        let alerts = UIAlertController(
            title: NSLocalizedString("Update Qty",
                                     comment: ""),
            message: "",
            preferredStyle: .alert)

        alerts.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.keyboardType = .decimalPad
            textField.delegate = self
        })
        
        alerts.addAction(UIAlertAction(title: NSLocalizedString("Cancel",
                                                                comment: ""),
                                       style: .cancel,
                                       handler: nil))
       
        alerts.addAction(UIAlertAction(title: NSLocalizedString("OK",
                                                                comment: ""),
                                       style: .default,
                                       handler: { (_) in
         
            if let qty = alerts.textFields?[0].text
            {
                self.updateQtyToItem(qty: qty , tempTaskId: tempTaskId )
            }
            
        }))

        self.present(alerts, animated: true, completion: nil)
    }
    
    func updateHeadingInfo( location: String , date: String){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Planstypechecklist")
        
        entity.predicate = NSPredicate(format:"plans_id = %@", plans_id)
        
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count != 0{
            
            let dic = result![0]
            dic.setValue(location, forKeyPath: "destination")
            dic.setValue(date, forKeyPath: "date")
            
            do {
                try managedContext.save()
                loadData()
            } catch _ as NSError {
                Helper.app.failedAlert( NSLocalizedString("Failed To Update",
                                                          comment: ""),
                                        vc: self)
               
            }
        }
    }
    
    func updateQtyToItem( qty: String , tempTaskId: String){
  
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Planstypechecklist")
        
        entity.predicate = NSPredicate(format:"item_id = %@", tempTaskId)
        
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count == 1 {
            
            let dic = result![0]
            
            dic.setValue(qty, forKeyPath: "qty")
        
            do {
                try managedContext.save()
                Helper.app.successAlert( NSLocalizedString("Successfully Added",
                                                           comment: ""),
                                         vc: self)
            } catch _ as NSError {
                Helper.app.failedAlert( NSLocalizedString("Failed To Update",
                                                          comment: ""),
                                        vc: self)
              
            }
        }
    }
    
    @IBAction func addNewChecklist(_ sender: Any) {
   
        let locationText: String = locationTextField.text!
        let dateText: String = datePickerTextfield.text!
        plans_id = Helper.app.random(27)
        
        if locationText.isEmpty {
            
            Helper.app.warningAlert( NSLocalizedString("Confirm",
                                                        comment: ""),
                                    msg: NSLocalizedString("",
                                                         comment: ""),
                                    vc: self)
        }else{
            
            if(ranking.count < 1){
                ranking.insert(DbHelper.app.convertDateToTimeStamp(Date()), at: 0)
            }
            
            let status = DbHelper.app.addPlan(destination: locationText,
                                              date: dateText,
                                              plans_id: plans_id,
                                              trip_id: trip_id,
                                              typeReceived: typeReceived,
                                              rank:ranking.first as Any as! Double
            )
            if(status == true){
                
                addNewChecklistButton.isEnabled = false
                addNewChecklistButton.image = nil
                
                addItemsButton.isEnabled = true
                addItemsButton.image = UIImage(named: "icons8-plus-48.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
                
            }else{
                Helper.app.failedAlert( NSLocalizedString("Failed",
                                                          comment: ""),
                                        vc: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal, title:
            NSLocalizedString("Delete",
                              comment: ""
        )) { (rowAction, indexPath) in

            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            managedContext.delete(self.MyChecklists[indexPath.row] as NSManagedObject)
            do {
                try managedContext.save()
                
                self.loadData()
            } catch _ {
                
            }
           
        }
        deleteAction.backgroundColor = .red
        return [deleteAction]
    }
        
    func updateItemIdDone( task:Planstypechecklist, isDone: Bool){
        
        let itemsId: String = (task.value(forKeyPath: "item_id") as? String)!
        print(itemsId)
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
 
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Planstypechecklist")
        
        entity.predicate = NSPredicate(format:"item_id = %@", itemsId)
        
        
        let result = try? managedContext.fetch(entity)
        print(result?.count as Any)
        if result?.count == 1 {
            
            let dic = result![0]
            dic.setValue(isDone, forKeyPath: "isdone")

        do {
            try managedContext.save()
            loadData()
            }  catch _ as NSError {
                Helper.app.failedAlert( NSLocalizedString("Failed To Update",
                                                          comment: ""),
                                        vc: self)
            }
       }
    }
    
    func addNewToDoItem( title: String ){
        let locationText: String = locationTextField.text!
        let dateText: String = datePickerTextfield.text!
        let item_id = Helper.app.random(29)
       
        
       let status = DbHelper.app.addChecklist(item_id: item_id, date: dateText, destination: locationText, qty: "1", task: title, plans_id: plans_id, title: locationText, trip_id: trip_id, typeReceived: typeReceived, isDone: false )
        
        if(status == true){
              loadData()
        }else{
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool{
        
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        return true
    }
}
