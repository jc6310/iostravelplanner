//
//  CustomMainTableViewCell.swift
//  travelplanner
//
//  Created by James Costello on 21/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit

class CustomMainTableViewCell: UITableViewCell {

    @IBOutlet weak var viewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
