//
//  FormsTwoViewController.swift
//  travelplanner
//  https://danilovdev.blogspot.com/2017/11/uidatepicker-as-input-view-for.html
//  Created by James Costello on 31/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit
import CoreData

class FormsTwoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    var typeReceived = ""
    var trip_id = ""
    var plans_id = ""
    var isEditReceived = ""
    var ranking : [Double] = []
    
    @IBOutlet weak var locationTextfield: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addNewItinerary: UIBarButtonItem!
    @IBOutlet weak var addItemsToItinerary: UIBarButtonItem!
    
    let datePickerView:UIDatePicker = UIDatePicker()
    var MyItinerary = [Planstypeitinerary]()
    var detailReceived = [Myplans]()
    var tripDetail = [Mytrips]()
    var fetchedResultsController: NSFetchedResultsController<Planstypeitinerary>!
    
    var moc:NSManagedObjectContext!
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        self.dateField.inputView = self.datePickerView
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        self.dateField.inputAccessoryView = self.createToolbar()
        
        moc = appDel.persistentContainer.viewContext
        
        if(typeReceived=="Daily Itinerary" || typeReceived=="Itinerary Notes"){
            title = typeReceived
            addNewItinerary.isEnabled = true
            addNewItinerary.image = UIImage(named: "icons8-save-all-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            addItemsToItinerary.isEnabled = false
            addItemsToItinerary.image = nil
        }else{
            title = (detailReceived.first?.value(forKeyPath: "destination") as? String)!
            print(detailReceived)
            addNewItinerary.isEnabled = false
            addNewItinerary.image = nil
            
            addItemsToItinerary.isEnabled = true
            addItemsToItinerary.image = UIImage(named: "icons8-plus-48.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            if(isEditReceived == "view"){
                locationTextfield.isEnabled = false
                dateField.isEnabled = false
            }
            
            plans_id = (detailReceived.first?.value(forKeyPath: "plans_id") as? String)!
            
            locationTextfield.text =  detailReceived.first?.value(forKeyPath: "destination") as? String
            dateField.text =  detailReceived.first?.value(forKeyPath: "date") as? String
            locationTextfield.isEnabled = false
            dateField.isEnabled = false
            loadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#0A0D20")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        addItemsToItinerary.width = CGFloat(32)
        addNewItinerary.width = CGFloat(32)

    }
    
    func loadData(){
        
        let MyItineraryRequest:NSFetchRequest<Planstypeitinerary> = Planstypeitinerary.fetchRequest()
        
        let categorySort =
            NSSortDescriptor(key: "category", ascending: true)
        
        MyItineraryRequest.sortDescriptors = [categorySort]
        MyItineraryRequest.predicate = NSPredicate(format:"plan_id = %@", plans_id)
        
        fetchedResultsController =
            NSFetchedResultsController(fetchRequest: MyItineraryRequest,
                                       managedObjectContext: moc,
                                       sectionNameKeyPath: "category",
                                       cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
            self.tableView.reloadData()

        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title:NSLocalizedString("Cancel", comment: ""),
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(hideDatePicker))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil, action: nil)
        let doneBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""),
                                                style: .done,
                                                target: self,
                                                action: #selector(doneDatePicker))
        toolbar.setItems([cancelBarButtonItem, flexBarButtonItem, doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    @objc func hideDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc func doneDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let selectedDate = self.datePickerView.date
        ranking.insert(selectedDate.timeIntervalSince1970, at: 0)
        self.dateField.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    func numberOfSections
        (in tableView: UITableView) -> Int {
        
        if fetchedResultsController != nil {
            return fetchedResultsController.sections!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        let sectionInfo =
            fetchedResultsController.sections![section]
        return sectionInfo.name
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        return fetchedResultsController.sections?[section].objects!.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        height = 50
        
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ItineraryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! ItineraryTableViewCell
        
        if let note = fetchedResultsController.object(at: indexPath) as Planstypeitinerary? {
            cell.notesLabel?.text = note.notes
        }
        cell.layer.borderWidth = CGFloat(3)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .normal,
                                                title:  NSLocalizedString("Delete", comment: ""))
        { (rowAction, indexPath) in
            
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            let managedContext = appDelegate.persistentContainer.viewContext
            managedContext.delete(self.fetchedResultsController.object(at: indexPath)  as NSManagedObject)
            do {
                try managedContext.save()
                
                self.loadData()
            } catch _ {
                
            }
        }
        deleteAction.backgroundColor = .red
        return [deleteAction]
    }
    
    @IBAction func addNewItineray(_ sender: Any) { 

        let locationText: String = locationTextfield.text!
        let dateText: String = dateField.text!
        plans_id = Helper.app.random(27)
        
        if(ranking.count < 1){
            ranking.insert(DbHelper.app.convertDateToTimeStamp(Date()), at: 0)
        }
        
        let status = DbHelper.app.addPlan(destination: locationText,
                                          date: dateText,
                                          plans_id: plans_id,
                                          trip_id: trip_id,
                                          typeReceived: typeReceived,
                                          rank: ranking.first as Any as! Double
        )
        
        if(status == true){
            addNewItinerary.isEnabled = false
            addNewItinerary.image = nil
            
            addItemsToItinerary.isEnabled = true
            addItemsToItinerary.image = UIImage(named: "icons8-plus-48.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed",
                                                      comment: ""),
                                    vc: self)
        }
    }
    
    @IBAction func addNewNotes(_ sender: Any) {
   
        
        let alert = UIAlertController(title: "",
                                      message: "",
                                      preferredStyle: .alert)
        
        let notesAction = UIAlertAction(title: NSLocalizedString("Notes",  comment: ""),
                                        style: .default,
                                        handler: { (action) -> Void in
            self.AddNotesToItinerary(category: "Notes")
        })
        
        let scheduleAction = UIAlertAction(title: NSLocalizedString("Schedule", comment: ""),
                                           style: .default,
                                           handler: { (action) -> Void in
            self.AddNotesToItinerary(category: "Schedule")
        })
        
        let plansAction = UIAlertAction(title: NSLocalizedString("Plans", comment: ""),
                                        style: .default,
                                        handler: { (action) -> Void in
            self.AddNotesToItinerary(category: "Plans")
        })
        
        let cancel = UIAlertAction(title:  NSLocalizedString("Cancel",  comment: ""),
                                   style: .destructive,
                                   handler: { (action) -> Void in })
        
        alert.view.tintColor = UIColor.black
        alert.view.backgroundColor = UIColor.white
        alert.view.layer.cornerRadius = 25
        
        alert.addAction(notesAction)
        alert.addAction(scheduleAction)
        alert.addAction(plansAction)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func AddNotesToItinerary(category:String){
        
        let alert = UIAlertController(title: "",
                                      message: "",
                                      preferredStyle: .alert)
       
        let textfield = UIAlertAction(title: NSLocalizedString("OK",
                                                               comment: ""),
                                      style: .default,
                                      handler: { (action) -> Void in
            if let text = alert.textFields?[0].text
            {
                self.addNoteToItem(text: text , category: category)
            }
        })
        
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
            textField.autocorrectionType = .default
            textField.placeholder = NSLocalizedString("Enter Text", comment: "")
            textField.textColor = UIColor.green
        }

        // Cancel button
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                   style: .destructive, handler: { (action) -> Void in })
        

        alert.view.tintColor = UIColor.black
        alert.view.backgroundColor = UIColor.white
        alert.view.layer.cornerRadius = 25
        alert.addAction(textfield)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func addNoteToItem(text: String , category: String){
        
        let locationText: String = locationTextfield.text!
        let dateText: String = dateField.text!
        let item_id = Helper.app.random(27)
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Planstypeitinerary",
                                       in: managedContext)!
        
        let trip = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        trip.setValue(item_id, forKeyPath: "item_id")
        trip.setValue(dateText, forKeyPath: "date")
        trip.setValue(locationText, forKeyPath: "destination")
        trip.setValue(category, forKeyPath: "category")
        trip.setValue(plans_id, forKeyPath: "plan_id")
        trip.setValue(text, forKeyPath: "notes")
        trip.setValue(trip_id, forKeyPath: "trip_id")
        trip.setValue(typeReceived, forKeyPath: "type")
        
        do {
            try managedContext.save()
            loadData()
        }  catch _ as NSError {
            Helper.app.failedAlert( NSLocalizedString("Failed To Add Plan", comment: ""),
                                    vc: self)
        }
    }
    
    func updateHeadingInfo( location: String , date: String){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Planstypechecklist")
        
        entity.predicate = NSPredicate(format:"plan_id = %@", plans_id)
        
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count != 0{
            
            let dic = result![0]
            dic.setValue(location, forKeyPath: "destination")
            dic.setValue(date, forKeyPath: "date")
            
            do {
                try managedContext.save()
                
                loadData()
            }  catch _ as NSError {
                Helper.app.failedAlert( NSLocalizedString("Failed To Update", comment: ""),
                                        vc: self)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "itineraryToPlans" {
            let viewController = segue.destination as! SecondViewController
            viewController.detailReceived = self.tripDetail
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
