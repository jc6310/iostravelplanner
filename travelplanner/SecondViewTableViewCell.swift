//
//  SecondViewTableViewCell.swift
//  travelplanner
//
//  Created by James Costello on 25/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit

class SecondViewTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var clickLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
