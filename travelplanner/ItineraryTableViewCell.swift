//
//  ItineraryTableViewCell.swift
//  
//
//  Created by James Costello on 12/01/2019.
//

import UIKit

class ItineraryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
