//
//  TransportViewController.swift
//  travelplanner
//
//  Created by James Costello on 20/02/2019.
//  Copyright © 2019 James Costello. All rights reserved.
//

import UIKit
import CoreData

class TransportViewController: UIViewController, UITextFieldDelegate {
    
    var typeReceived = ""
    var detailReceived = [Myplans]()
    var MyOtherPlanslists = [Planstypeother]()
    let datePickerView:UIDatePicker = UIDatePicker()
    let datePickerDepartureView:UIDatePicker = UIDatePicker()
    var ranking : [Double] = []
    var tripDetail = [Mytrips]()
    var trip_id = ""
    var plans_id = ""
    var isEditReceived = ""
    var moc:NSManagedObjectContext!
    let cellReuseIdentifier = "cell"
    
    @IBOutlet weak var addFormButton: UIBarButtonItem!
    @IBOutlet weak var updateFormButton: UIBarButtonItem!
    @IBOutlet weak var departureText: UITextField!
    @IBOutlet weak var departureDateField: UITextField!
    @IBOutlet weak var arrivalText: UITextField!
    @IBOutlet weak var arrivalDateField: UITextField!
    @IBOutlet weak var reservationField: UITextField!
    @IBOutlet weak var transportSegented: UISegmentedControl!
    @IBOutlet weak var detailTextarea: UITextView!
    
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moc = appDel.persistentContainer.viewContext
        
        self.arrivalDateField.inputView = self.datePickerView
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        self.arrivalDateField.inputAccessoryView = self.createToolbar()
        self.departureDateField.inputView = self.datePickerDepartureView
        datePickerDepartureView.datePickerMode = UIDatePicker.Mode.date
        self.departureDateField.inputAccessoryView = self.createToolbarDeparture()
        
        title = typeReceived
        if(typeReceived=="Transportion"){
            
            addFormButton.isEnabled = true
            addFormButton.image = UIImage(named: "icons8-save-all-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            updateFormButton.isEnabled = false
            updateFormButton.image = nil

        }else{
         
            title =  (detailReceived.first?.value(forKeyPath: "type") as? String)!
            plans_id = (detailReceived.first?.value(forKeyPath: "plans_id") as? String)!
            ranking.insert( (detailReceived.first?.value(forKeyPath: "rank") as? Double)!, at: 0)
 
            addFormButton.isEnabled = false
            addFormButton.image = nil
            
            updateFormButton.isEnabled = true
            updateFormButton.image = UIImage(named: "icons8-update-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)

            loadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#0A0D20")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
    
    func loadData(){
        
        let MyplansOthersRequest:NSFetchRequest<Planstypeother> = Planstypeother.fetchRequest()
        
        MyplansOthersRequest.predicate = NSPredicate(format:"%K == %@", "plan_id", plans_id)
        
        do {
            try MyOtherPlanslists = moc.fetch(MyplansOthersRequest)
        } catch {
            Helper.app.failedAlert(NSLocalizedString("FailedToLoadData",
                                                     comment: ""),
                                   vc: self)
        }
        populatData()
    }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel",comment: ""),
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(hideDatePicker))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil,
                                                action: nil)
        let doneBarButtonItem = UIBarButtonItem(title:  NSLocalizedString("Done",
                                                                          comment: ""),
                                                style: .done,
                                                target: self,
                                                action: #selector(doneDatePicker))
        toolbar.setItems([cancelBarButtonItem,
                          flexBarButtonItem,
                          doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    func createToolbarDeparture() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel",
                                                                           comment: ""),
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(hideDatePickerDeparture))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil,
                                                action: nil)
        let doneBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done",
                                                                         comment: ""),
                                                style: .done,
                                                target: self,
                                                action: #selector(doneDatePickerDeparture))
        toolbar.setItems([cancelBarButtonItem,
                          flexBarButtonItem,
                          doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    @objc func hideDatePickerDeparture() {
        self.view.endEditing(true)
    }
    
    @objc func hideDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc func doneDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let selectedDate = self.datePickerView.date
        ranking.insert(selectedDate.timeIntervalSince1970, at: 0)
        self.arrivalDateField.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    @objc func doneDatePickerDeparture() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let selectedDate = self.datePickerDepartureView.date
        ranking.insert(selectedDate.timeIntervalSince1970, at: 0)
        self.departureDateField.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
 
    func populatData(){
  
         self.departureText.text = MyOtherPlanslists.first?.value(forKeyPath: "departureAddress") as? String
        
         self.arrivalText.text = MyOtherPlanslists.first?.value(forKeyPath: "arrivalAddress") as? String
        
        self.reservationField.text = MyOtherPlanslists.first?.value(forKeyPath: "reservationNo") as? String
        
        self.arrivalDateField.text = MyOtherPlanslists.first?.value(forKeyPath: "arrivalDate") as? String
        
        self.departureDateField.text = MyOtherPlanslists.first?.value(forKeyPath: "departureDate") as? String
        self.detailTextarea.text = MyOtherPlanslists.first?.value(forKeyPath: "details") as? String
        
        let transport = MyOtherPlanslists.first?.value(forKeyPath: "transport") as? String
         transportSegented.selectedSegmentIndex = setTransportSelected(transport: transport!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transportToPLans" {
            let viewController = segue.destination as! SecondViewController
            viewController.detailReceived = self.tripDetail
        }
    }
    
    @IBAction func saveTransportForm(_ sender: Any) {
        
        let departureText: String = self.departureText.text!
        let departureDateField: String = self.departureDateField.text!
        plans_id = Helper.app.random(27)
        
        if departureText.isEmpty {
            Helper.app.warningAlert( NSLocalizedString("Confirm",
                                                       comment: ""),
                                     msg: NSLocalizedString("Destination Name is Required",
                                                            comment: ""),
                                     vc: self)
        }else{
            
            if(ranking.count < 1){
                ranking.insert(DbHelper.app.convertDateToTimeStamp(Date()), at: 0)
            }
            
            let status = DbHelper.app.addPlan(destination: departureText,
                                 date: departureDateField,
                                 plans_id: plans_id,
                                 trip_id: trip_id,
                                 typeReceived: typeReceived,
                                 rank: ranking.first as Any as! Double
             )
            
            if(status == true){
                saveNewForm()
            }else{
                Helper.app.failedAlert( NSLocalizedString("Failed To Add Plan",
                                                          comment: ""),
                                        vc: self)
            }
         
         }
    }
    
    func saveNewForm() {
        
        let arrivalText: String = self.arrivalText.text!
        let departureText: String = self.departureText.text!
        let reservationText: String = reservationField.text!
        let detailTextarea: String = self.detailTextarea.text!
        let arrivalDateText: String = arrivalDateField.text!
        let departureDateText: String = departureDateField.text!
        let transportControls = getTransportSelected()
        let empty = ""
      
        let status = DbHelper.app.addTransportAcco(accommodationName: empty,
                                       arrivalDate: arrivalDateText,
                                       arrivalAddress: arrivalText,
                                       departureAddress: departureText,
                                       departureDate: departureDateText,
                                       numberOfNights: empty,
                                       plan_id: plans_id,
                                       reservationNo: reservationText,
                                       transport: transportControls,
                                       trip_id: trip_id,
                                       type: title!,
                                       details:  detailTextarea
        )
       
        if(status == true){
            addFormButton.isEnabled = false
            addFormButton.image = nil
            
            updateFormButton.isEnabled = true
            updateFormButton.image = UIImage(named: "icons8-update-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)

            
            Helper.app.successAlert( NSLocalizedString("Successfully Added",
                                                       comment: ""),
                                     vc: self)
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Add Plan",
                                                      comment: ""),
                                    vc: self)
        }
    }
    
    @IBAction func updateForm(_ sender: Any) {
        
        let arrivalText: String = self.arrivalText.text!
        let departureText: String = self.departureText.text!
        let reservationText: String = reservationField.text!
        let detailTextarea: String = self.detailTextarea.text!
        let arrivalDateText: String = arrivalDateField.text!
        let departureDateText: String = departureDateField.text!
        let transportControls = getTransportSelected()
        let empty = ""
        
        let status = DbHelper.app.updateTransportAcco(accommodationName: empty,
                                      arrivalDate: arrivalDateText,
                                      arrivalAddress: arrivalText,
                                      departureAddress: departureText,
                                      departureDate: departureDateText,
                                      numberOfNights: empty,
                                      plan_id: plans_id,
                                      reservationNo: reservationText,
                                      transport: transportControls,
                                      trip_id: trip_id,
                                      type: title!,
                                      details:  detailTextarea
        )
        
        if(status == true){
            updatePlanForm()
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Update Plan",
                                                      comment: ""),
                                    vc: self)
        }
     
    }
    
    func updatePlanForm() {
        
        let departureText: String = self.departureText.text!
        let arrivalDateField: String = self.arrivalDateField.text!
        if(ranking.count < 1){
            ranking.insert(DbHelper.app.convertDateToTimeStamp(Date()), at: 0)
        }
        
        let status = DbHelper.app.updatePlan(destination: departureText,
                                 date: arrivalDateField,
                                 plans_id: plans_id,
                                 rank: ranking.first as Any as! Double
        )
        
        if(status == true){
            Helper.app.successAlert( NSLocalizedString("Successfully Updated",
                                                       comment: ""),
                                     vc: self)
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Update Plan",
                                                      comment: ""),
                                    vc: self)
        }
    }
    
    func setTransportSelected(transport:String) -> Int {
        
        var transportOptions:[String] = [NSLocalizedString("Bus",comment: ""),
                                         NSLocalizedString("Boat",comment: ""),
                                         NSLocalizedString("Car",comment: ""),
                                         NSLocalizedString("Plane",comment: ""),
                                         NSLocalizedString("Other",comment: "") ]
        var index = 0
        
        for i in 0..<transportOptions.count {
            if transportOptions[i].contains(transport) {
                index = i
            }
        }
        return index
    }
    
    func getTransportSelected() -> String {
        
        var transportOptions:[String] = [NSLocalizedString("Bus",comment: ""),
                                         NSLocalizedString("Boat",comment: ""),
                                         NSLocalizedString("Car",comment: ""),
                                         NSLocalizedString("Plane",comment: ""),
                                         NSLocalizedString("Other",comment: "") ]
        let selectTransport: Int = transportSegented.selectedSegmentIndex
        
        return transportOptions[selectTransport]
        
    }
}
