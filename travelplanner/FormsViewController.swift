//
//  FormsViewController.swift
//  travelplanner
//
//  Created by James Costello on 02/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit
import CoreData


class FormsViewController: UIViewController, UITextFieldDelegate {
    
    var typeReceived = ""
    var detailReceived = [Myplans]()
    var MyOtherPlanslists = [Planstypeother]()
    let datePickerView:UIDatePicker = UIDatePicker()
    var tripDetail = [Mytrips]()
    var trip_id = ""
    var plans_id = ""
    var ranking : [Double] = []
    var isEditReceived = ""
    var moc:NSManagedObjectContext!
    let cellReuseIdentifier = "cell"
    
    @IBOutlet weak var reservationField: UITextField!
    @IBOutlet weak var hotelAddressField: UITextField!
    @IBOutlet weak var accommadationNameText: UITextField!
    @IBOutlet weak var arrivalDateField: UITextField!
    @IBOutlet weak var noOfNightsText: UITextField!
    @IBOutlet weak var detailsText: UITextView!
    @IBOutlet weak var updateFormButton: UIBarButtonItem!
    @IBOutlet weak var addFormButton: UIBarButtonItem!
    
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        moc = appDel.persistentContainer.viewContext
        
        self.arrivalDateField.inputView = self.datePickerView
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        self.arrivalDateField.inputAccessoryView = self.createToolbar()
        
        title = typeReceived
        if(typeReceived=="Accommadation"){

            
            addFormButton.isEnabled = true
            addFormButton.image = UIImage(named: "icons8-save-all-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            updateFormButton.isEnabled = false
            updateFormButton.image = nil
        }else{
            title =  (detailReceived.first?.value(forKeyPath: "type") as? String)!
            plans_id = (detailReceived.first?.value(forKeyPath: "plans_id") as? String)!
            ranking.insert( (detailReceived.first?.value(forKeyPath: "rank") as? Double)!, at: 0)
            
            addFormButton.isEnabled = false
            addFormButton.image = nil
            
            updateFormButton.isEnabled = true
            updateFormButton.image = UIImage(named: "icons8-update-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
          loadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#0A0D20")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        updateFormButton.width = CGFloat(32)
        addFormButton.width = CGFloat(32)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func loadData(){
        
        let MyplansOthersRequest:NSFetchRequest<Planstypeother> = Planstypeother.fetchRequest()
        
        MyplansOthersRequest.predicate = NSPredicate(format:"%K == %@", "plan_id", plans_id)
        
        do {
            try MyOtherPlanslists = moc.fetch(MyplansOthersRequest)
        } catch {
            Helper.app.failedAlert(NSLocalizedString("FailedToLoadData",
                                                     comment: ""),
                                   vc: self)
        }
         populatData()
     }
    
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel",comment: ""),
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(hideDatePicker))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil,
                                                action: nil)
        let doneBarButtonItem = UIBarButtonItem(title:
            NSLocalizedString("Done",  comment: ""),
                                                style: .done,
                                                target: self,
                                                action: #selector(doneDatePicker))
        toolbar.setItems([cancelBarButtonItem, flexBarButtonItem, doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    @objc func hideDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc func doneDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        let selectedDate = self.datePickerView.date

        ranking.insert(selectedDate.timeIntervalSince1970, at: 0)
        
        self.arrivalDateField.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    func populatData(){

        self.accommadationNameText.text = MyOtherPlanslists.first?.value(forKeyPath: "accommodationName") as? String
        
        self.hotelAddressField.text = MyOtherPlanslists.first?.value(forKeyPath: "arrivalAddress") as? String
    
        self.reservationField.text = MyOtherPlanslists.first?.value(forKeyPath: "reservationNo") as? String
        
        self.arrivalDateField.text = MyOtherPlanslists.first?.value(forKeyPath: "arrivalDate") as? String
        
        self.noOfNightsText.text = MyOtherPlanslists.first?.value(forKeyPath: "numberOfNights") as? String
        
        self.detailsText.text = MyOtherPlanslists.first?.value(forKeyPath: "details") as? String
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoToPlans" {
            let viewController = segue.destination as! SecondViewController
            viewController.detailReceived = self.tripDetail
        }
    }
    
    @IBAction func saveNewForm(_ sender: Any) {
        
        let accommadationNameText: String = self.accommadationNameText.text!
        let arrivalDateField: String = self.arrivalDateField.text!
        plans_id = Helper.app.random(27)
        
        if accommadationNameText.isEmpty {
            
            Helper.app.warningAlert( NSLocalizedString("Confirm",
                                                       comment: ""),
                                     msg: NSLocalizedString("Accommadation Name is Required",
                                                            comment: ""),
                                    vc: self)
        }else{
            
            if(ranking.count < 1){
                  ranking.insert(DbHelper.app.convertDateToTimeStamp(Date()), at: 0)
            }
            
           let status = DbHelper.app.addPlan(destination: accommadationNameText,
                                 date: arrivalDateField,
                                 plans_id: plans_id,
                                 trip_id: trip_id,
                                 typeReceived: typeReceived,
                                 rank: ranking.first as Any as! Double
           )
           if(status == true){
               saveNewForm()
            }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Add Plan",
                                                      comment: ""),
                                    vc: self)
            }
          }
    }
    
    func saveNewForm() {
        
        let accommadationNameText: String = self.accommadationNameText.text!
        let reservationText: String = reservationField.text!
        let hotelAddressField: String = self.hotelAddressField.text!
        let detailsText: String = self.detailsText.text!
        let arrivalDateText: String = arrivalDateField.text!
        let noOfNightsText: String = self.noOfNightsText.text!
        let empty = ""
        
        let status = DbHelper.app.addTransportAcco(accommodationName: accommadationNameText,
                                       arrivalDate: arrivalDateText,
                                       arrivalAddress: hotelAddressField,
                                       departureAddress: empty,
                                       departureDate: empty,
                                       numberOfNights: noOfNightsText,
                                       plan_id: plans_id,
                                       reservationNo: reservationText,
                                       transport: empty,
                                       trip_id: trip_id,
                                       type: title!,
                                       details: detailsText
        )
        
        if(status == true){
            
            addFormButton.isEnabled = false
            addFormButton.image = nil
            
            updateFormButton.isEnabled = true
            updateFormButton.image = UIImage(named: "icons8-update-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)

          
            Helper.app.successAlert( NSLocalizedString("Successfully Added",
                                                       comment: ""),
                                     vc: self)
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Add Plan",
                                                      comment: ""),
                                    vc: self)
        }
    }
    
    @IBAction func updateForm(_ sender: Any) {
        
        let accommadationNameText: String = self.accommadationNameText.text!
        let reservationText: String = reservationField.text!
        let detailsText: String = self.detailsText.text!
        let arrivalDateText: String = arrivalDateField.text!
        let noOfNightsText: String = self.noOfNightsText.text!
        let hotelAddressField: String = self.hotelAddressField.text!
        let empty = ""
     
        let status = DbHelper.app.updateTransportAcco(accommodationName: accommadationNameText,
                                      arrivalDate: arrivalDateText,
                                      arrivalAddress: hotelAddressField,
                                      departureAddress: empty,
                                      departureDate: empty,
                                      numberOfNights: noOfNightsText,
                                      plan_id: plans_id,
                                      reservationNo: reservationText,
                                      transport: empty,
                                      trip_id: trip_id,
                                      type: title!,
                                      details: detailsText
        )
        
        if(status == true){
            updatePlanForm()
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Update Plan",
                                                      comment: ""),
                                    vc: self)
        }
    }
    
    func updatePlanForm()
    {
        let accommadationNameText: String = self.accommadationNameText.text!
        let arrivalDateField: String = self.arrivalDateField.text!
        
        if(ranking.count < 1){
            ranking.insert(DbHelper.app.convertDateToTimeStamp(Date()), at: 0)
        }
        
        let status = DbHelper.app.updatePlan(destination: accommadationNameText,
                                date: arrivalDateField,
                                plans_id: plans_id,
                                rank: ranking.first as Any as! Double
        )
        
        if(status == true){
            Helper.app.successAlert( NSLocalizedString("Successfully Updated",
                                                       comment: ""),
                                     vc: self)
        }else{
            Helper.app.failedAlert( NSLocalizedString("Failed To Update Plan",
                                                      comment: ""),
                                    vc: self)
        }
    }
}
