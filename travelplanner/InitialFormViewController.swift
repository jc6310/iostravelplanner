//
//  InitialFormViewController.swift
//  travelplanner
//
//  Created by James Costello on 21/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit
import CoreData

class InitialFormViewController: UIViewController, UITextFieldDelegate {
    
    var detailReceived = [Mytrips]()
    let returningDatePickerView:UIDatePicker = UIDatePicker()
    let departureDatePickerView:UIDatePicker = UIDatePicker()
    let finishDatePickerView:UIDatePicker = UIDatePicker()
    var introHeading = ""
    var errorMsgNamerequired = NSLocalizedString("NameFieldRequired", comment: "")
    var errorHeaderNamerequired = NSLocalizedString("Oops", comment: "")
    
    @IBOutlet weak var updateButton: UIBarButtonItem!
    @IBOutlet weak var createButton: UIBarButtonItem!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var destinationField: UITextField!
    @IBOutlet weak var departureDateField: UITextField!
    @IBOutlet weak var returningDateField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        destinationField.delegate = self
        
        self.departureDateField.inputView = self.departureDatePickerView
        departureDatePickerView.datePickerMode = UIDatePicker.Mode.date
        self.departureDateField.inputAccessoryView = self.createToolbar()
        
        self.returningDateField.inputView = self.returningDatePickerView
        returningDatePickerView.datePickerMode = UIDatePicker.Mode.date
        self.returningDateField.inputAccessoryView = self.createToolbarReturning()
    
        if detailReceived.isEmpty{
 
            createButton.isEnabled = true
            createButton.image = UIImage(named: "icons8-save-all-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            updateButton.isEnabled = false
            updateButton.image = nil
            
            introHeading = NSLocalizedString("AddTrip",  comment: "")
            
        }else{

            createButton.isEnabled = false
            createButton.image = nil
            
            updateButton.isEnabled = true
            updateButton.image = UIImage(named: "icons8-update-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
            
            nameTextField.text =  detailReceived.first?.value(forKeyPath: "name") as? String
            destinationField.text = detailReceived.first?.value(forKeyPath: "destination") as? String
            departureDateField.text = detailReceived.first?.value(forKeyPath: "departuredate") as? String
            returningDateField.text = detailReceived.first?.value(forKeyPath: "finisheddate") as? String
            introHeading = NSLocalizedString("UpdateTrip", comment: "")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBar.barTintColor = UIColor( hexString: "#0A0D20")
            title = introHeading
             updateButton.width = CGFloat(32)
             createButton.width = CGFloat(32)
    }
    
    @IBAction func createTrip(_ sender: Any) {
        
        let nameText: String = nameTextField.text!
     
        if nameText.isEmpty {
            Helper.app.warningAlert(errorHeaderNamerequired, msg: errorMsgNamerequired, vc: self)
        }else{
           createTrips()
        }
    }
    
    @IBAction func updateTrip(_ sender: Any) {
        
        let nameText: String = nameTextField.text!
        
        if nameText.isEmpty {
              Helper.app.warningAlert(errorHeaderNamerequired, msg: errorMsgNamerequired, vc: self)
        }else{
            updateTrips();
        }
    }
   
    func createToolbar() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title:
            NSLocalizedString("Cancel",
                              comment: ""),
                                                  style: .plain,
                                                  target: self, action: #selector(hideDatePicker))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil, action: nil)
        let doneBarButtonItem = UIBarButtonItem(title:
            NSLocalizedString("Done",
                              comment: ""),
                                                style: .done,
                                                target: self, action: #selector(doneDatePicker))
        toolbar.setItems([cancelBarButtonItem, flexBarButtonItem, doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    func createToolbarReturning() -> UIToolbar {
        let toolbar = UIToolbar()
        let cancelBarButtonItem = UIBarButtonItem(title:
            NSLocalizedString("Cancel",
                              comment: ""),
                                                  style: .plain,
                                                  target: self, action: #selector(hideDatePickerReturning))
        let flexBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil, action: nil)
        let doneBarButtonItem = UIBarButtonItem(title:
            NSLocalizedString("Done",
                              comment: ""),
                                                style: .done,
                                                target: self, action: #selector(doneDatePickerReturning))
        toolbar.setItems([cancelBarButtonItem, flexBarButtonItem, doneBarButtonItem],
                         animated: false)
        
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.black
        toolbar.isTranslucent = false
        toolbar.isUserInteractionEnabled = true
        toolbar.sizeToFit()
        
        return toolbar
    }
    
    @objc func hideDatePickerReturning() {
        self.view.endEditing(true)
    }
    
    @objc func hideDatePicker() {
        self.view.endEditing(true)
    }
    
    @objc func doneDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let selectedDate = self.departureDatePickerView.date
        self.departureDateField.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    @objc func doneDatePickerReturning() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let selectedDate = self.returningDatePickerView.date
        self.returningDateField.text = dateFormatter.string(from: selectedDate)
        self.view.endEditing(true)
    }
    
    func createTrips()  {
        
        let nameText: String = nameTextField.text!
        let destinationText: String = destinationField.text!
        let departureDate: String = departureDateField.text!
        let finishDate: String = returningDateField.text!
        let password: String = Helper.app.random(4)
        let unique_id: String = Helper.app.random(26)
        let id: String = Helper.app.random(6)
        
        
        let status = DbHelper.app.addTrip(name: nameText, destination: destinationText, departuredate: departureDate, finishDate: finishDate, password: password, unique_id: unique_id, id: id, rank: 0.0)
        
        if(status == true){
        
            Helper.app.successAlert(NSLocalizedString("SuccessfullyCreatedTrip",
                                                      comment: ""),
                                    vc: self)
            createButton.isEnabled = false
            createButton.image = nil
            
            updateButton.isEnabled = true
            updateButton.image = UIImage(named: "icons8-update-30.png")!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        }else{
            Helper.app.failedAlert(NSLocalizedString("FailedToCreateTrip",
                                                     comment: ""),
                                   vc: self)
        }
    }
    
    func updateTrips() {
        
        let nameText: String = nameTextField.text!
        let destinationText: String = destinationField.text!
        let departureDate: String = departureDateField.text!
        let finishDate: String = returningDateField.text!
        let unique_id: String =  (detailReceived.first?.value(forKeyPath: "unique_id") as? String)!
        
        let status = DbHelper.app.updateTrips(nameText: nameText, destinationText: destinationText, departureDate: departureDate, finishDate: finishDate, unique_id: unique_id)
        
        if(status == true){
             Helper.app.successAlert(NSLocalizedString("SuccessfullyUpdatedTrip",
                                                       comment: ""),
                                     vc: self)
        }else{
            Helper.app.failedAlert(NSLocalizedString("FailedToUpdatedTrip",
                                                     comment: ""),
                                   vc: self)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool  {
        
        let maxLength = 100
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
      }
    
}
