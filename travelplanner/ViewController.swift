//
//  ViewController.swift
//  travelplanner
//
//  Created by James Costello on 17/11/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//

import UIKit
import CoreData
import FirebaseDatabase
import Crashlytics

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var importBarButton: UIBarButtonItem!
    @IBOutlet weak var contactBarButton: UIBarButtonItem!
    @IBOutlet weak var addBarButton: UIBarButtonItem!
    
    var detailToSend = [Mytrips]()
    var names: [String] = []
    let perons:  [NSManagedObject] = []
    var ref:  DatabaseReference!
    var databaseHandle :  DatabaseHandle!
    
    var AllMyTrips = [Mytrips]()
    
    let cellReuseIdentifier = "cell"
    var moc:NSManagedObjectContext!
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
 
    override func viewDidLoad() {
        super.viewDidLoad()

        moc = appDel.persistentContainer.viewContext
        tableView.dataSource = self
        tableView.delegate = self
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#0A0D20")
         title = NSLocalizedString("My Trips", comment: "")
    
        addBarButton.width = CGFloat(32)
        contactBarButton.width = CGFloat(32)
        importBarButton.width = CGFloat(32)
    }
    
    func loadData(){
     
       let MytripsRequest:NSFetchRequest<Mytrips> = Mytrips.fetchRequest()
    
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: false)
        MytripsRequest.sortDescriptors = [sortDescriptor]
       
        MytripsRequest.relationshipKeyPathsForPrefetching = ["mytripsToMyPlans"]
        do {
            try AllMyTrips = moc.fetch(MytripsRequest)
        } catch {
            Helper.app.failedAlert(NSLocalizedString("FailedToLoadData", comment: ""),
                                   vc: self)
        }
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowPlanners" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let nav = segue.destination as! UINavigationController
                let controller = nav.topViewController as! SecondViewController
                
                controller.detailReceived =  [self.AllMyTrips[indexPath.row]]
            }
        }
        if segue.identifier == "showInitialFormSegue" {
            let nav = segue.destination as! UINavigationController
            let controller = nav.topViewController as! InitialFormViewController
            
            controller.detailReceived = detailToSend
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return AllMyTrips.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        height = 70
        
        return height
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let listMyTrips = AllMyTrips[indexPath.row]
    
        let cell:CustomMainTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomMainTableViewCell

        cell.nameLabel.text =
            listMyTrips.value(forKeyPath: "name") as? String
        
        cell.dateLabel.text =  listMyTrips.value(forKeyPath: "departuredate") as? String
 
        cell.layer.borderWidth = CGFloat(3)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
   
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "ShowPlanners", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
      
        let editAction = UITableViewRowAction(style: .normal, title: "Edit") { (rowAction, indexPath) in
            self.detailToSend = [self.AllMyTrips[indexPath.row]]
            
            self.performSegue(withIdentifier: "showInitialFormSegue", sender: self.detailToSend)
        }
        
        editAction.backgroundColor = .purple
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
           
        let alert = UIAlertController(title:NSLocalizedString("AreYouSureYouWantToDelete", comment: ""),
                                      message:NSLocalizedString("ThisIsPermanent", comment: ""), preferredStyle: .alert)
           
            alert.addAction(UIAlertAction(title:NSLocalizedString("Yes", comment: ""), style: .default,
                                          handler: { action in self.deletetrip(self.AllMyTrips[indexPath.row])
            } ))
            alert.addAction(UIAlertAction(title:NSLocalizedString("No", comment: ""), style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
        deleteAction.backgroundColor = .red

        let moreAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "More" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            
            self.moreOptions(passShow: self.AllMyTrips[indexPath.row].md5_pass ?? "")
        })
        moreAction.backgroundColor = .blue
        
        let syncAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Sync" , handler: { (action:UITableViewRowAction, indexPath: IndexPath) -> Void in
            // 2
          
            let alert = UIAlertController(title:NSLocalizedString("Are you sure you want to Sync?", comment: "") ,
                                          message:NSLocalizedString("ThisIsPermanent", comment: ""), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title:NSLocalizedString("Yes", comment: ""),
                                          style: .default,
                                          handler: { action in self.doSync(self.AllMyTrips[indexPath.row])
            } ))
            alert.addAction(UIAlertAction(title:NSLocalizedString("No", comment: ""),
                                          style: .cancel,
                                          handler: nil))
            
            self.present(alert, animated: true)
        })
        syncAction.backgroundColor = .orange
        
        return [syncAction, editAction, deleteAction, moreAction]
    }
    
    func deletetrip(_ AllMyTripsTemp: Mytrips) {
        
        let trip_idbe =  (AllMyTripsTemp.unique_id) ?? ""
        
        let status = DbHelper.app.deletetrip(trip_id: trip_idbe, AllMyTripsTemp)
  
       if(status==true){
        
          ref = Database.database().reference()
         ref.child("trip").child(trip_idbe).removeValue()
        
          self.loadData()
        
       }else{
         Helper.app.failedAlert( NSLocalizedString("Failed", comment:""),
                                 vc: self)
       }
      
    }
    func moreOptions(passShow: String){
        
        let alertController = UIAlertController(title: NSLocalizedString("More Options", comment: ""),
                                                message: nil,
                                                preferredStyle: UIAlertController.Style.actionSheet)
   
        let showPassForm = UIAlertAction(title: NSLocalizedString("Show Password", comment: ""),
                                         style: .default,
                                         handler: { action in
                                                
            let alert = UIAlertController(title:NSLocalizedString("Password", comment: ""),
                                          message:NSLocalizedString(passShow, comment: ""),
                                          preferredStyle: .alert)
                                                
               alert.addAction(UIAlertAction(title:NSLocalizedString("OK", comment: ""),
                                             style: .cancel,
                                             handler: nil))
                                                
              self.present(alert, animated: true)
        })
        
        let ItineraryForm = UIAlertAction(title: NSLocalizedString("Share",  comment: ""),
                                          style: .default,
                                          handler: { action in
                                    
           let text =    NSLocalizedString("This is some text that I want to share.", comment: "")
                                            
           let textToShare = [ text ]
           let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop]
                                            
           self.present(activityViewController, animated: true, completion: nil)
        })
        
      
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""),
                                         style: .cancel,
                                         handler: {(alert: UIAlertAction!) in print(
                                            NSLocalizedString("Cancel", comment: ""))
        })
        

        alertController.addAction(showPassForm)
        alertController.addAction(ItineraryForm)

        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion:{})
    }
    @IBAction func doImport(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("Import Trip", comment: ""),
                                      message: NSLocalizedString("Insert Trip Id and password", comment: ""),
                                      preferredStyle: .alert)
        let importAction = UIAlertAction(title: NSLocalizedString("Import Trip", comment: ""),
                                         style: .default,
                                         handler: { (action) -> Void in
            // Get TextFields text
            let tripIdTxt = alert.textFields![0].text
            let passwordTxt = alert.textFields![1].text

            self.doImportsCheck(trip_id: tripIdTxt! ,password: passwordTxt! )
          
        })
        
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
            textField.autocorrectionType = .default
            textField.placeholder =  NSLocalizedString("Type your trip id", comment: "")
            textField.textColor = UIColor.black
        }
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
            textField.placeholder =  NSLocalizedString("Type your password", comment: "")
            textField.isSecureTextEntry = true
            textField.textColor = UIColor.black
        }
        let cancelAction = UIAlertAction(title:  NSLocalizedString("Cancel", comment: ""),
                                         style: .cancel,
                                         handler: nil)
     
        alert.addAction(importAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func doImportsCheck(trip_id: String, password: String){

        let status = DbHelper.app.checkIfTripExists(trip_id)
        if(status==true){
           Helper.app.failedAlert(NSLocalizedString("Trip Already exists", comment: ""),
                                  vc: self)
        
        }else if(Helper.app.isInternetAvailable()){
   
            ref = Database.database().reference().child("trip").child(trip_id)
            
            ref.queryOrdered(byChild:  "md5_pass").queryStarting(atValue: password).queryEnding(atValue: password + "\u{f8ff}").observeSingleEvent(of: .value, with: { (snapshot) in
  
                if snapshot.exists(){
     
                    if let tripDict = snapshot.value as? NSDictionary {
       
                      if let tripMainDetails = tripDict[trip_id]  as? [String: Any] {
                            
                        _ = DbHelper.app.addTrip( name: tripMainDetails["name"] as! String,
                                              destination: tripMainDetails["destination"]  as! String,
                                              departuredate: tripMainDetails["departuredate"]  as! String,
                                              finishDate: tripMainDetails["finisheddate"] as! String ,
                                              password: tripMainDetails["md5_pass"] as! String,
                                              unique_id: tripMainDetails["unique_id"]  as! String,
                                              id: tripMainDetails["id"] as! String,
                                              rank: 0.0  )
                        
                       let plans = tripMainDetails["plans"] as! [[String:Any]]
                         for option in plans {
                                
                                let date = option["date"] as! String
                                let destination = option["destination"] as! String
                                let plans_id = option["plans_id"] as! String
                                let trip_id = option["trip_id"] as! String
                                let rank = option["rank"] as! String
                                let type = option["type"] as! String
                      
                            _ = DbHelper.app.addPlan( destination: destination,
                                                      date: date,
                                                      plans_id: plans_id,
                                                      trip_id: trip_id,
                                                      typeReceived: type,
                                                      rank: Double(rank) ?? 0.0)
                                
                                let planset = option["plan"] as! [[String:Any]]
                            
                                if(type=="Accommadation" || type=="Transportion" ){
                                   for plan in planset {
                             _ =  DbHelper.app.addTransportAcco(
                                accommodationName: plan["accommodationName"] as! String,
                                 arrivalDate: plan["arrivalDate"] as! String,
                                 arrivalAddress: plan["arrivalAddress"] as! String,
                                 departureAddress:plan["departureAddress"] as! String,
                                 departureDate: plan["departureDate"] as! String,
                                 numberOfNights: plan["numberOfNights"] as! String,
                                 plan_id: plan["plan_id"] as! String,
                                 reservationNo: plan["reservationNo"] as! String,
                                 transport: plan["transport"] as! String,
                                 trip_id: plan["trip_id"] as! String,
                                 type: plan["type"] as! String,
                                 details: plan["details"] as! String )
                                   }
                               }
                            
                               if(type=="Checklist" || type=="Pre Travel Checklist"){
                                  for plan in planset {
                                    
                                    var isdone = false
                                    if(plan["isdone"] as! String == "true"){
                                        isdone = true
                                    }
                                    
                                   _ =   DbHelper.app.addChecklist( item_id: plan["item_id"] as! String,
                                                                date: plan["date"] as! String,
                                                                destination: plan["destination"] as! String,
                                                                qty: plan["qty"] as! String,
                                                                task: plan["task"] as! String,
                                                                plans_id: plan["trip_id"] as! String,
                                                                title: plan["destination"] as! String,
                                                                trip_id: plan["trip_id"] as! String,
                                                                typeReceived: plan["type"] as! String,
                                                                isDone: isdone )
                                  }
                               }
                            
                               if(type=="Notes" || type=="Daily Itinerary" || type=="Itinerary Notes"){
                                  for plan in planset {
                                   _ =  DbHelper.app.addItinerary( item_id: plan["item_id"] as! String,
                                                               date: plan["date"] as! String,
                                                               destination: plan["destination"] as! String,
                                                               category: plan["category"] as! String,
                                                               title: plan["title"] as! String,
                                                               plans_id: plan["trip_id"] as! String,
                                                               text: plan["notes"] as! String,
                                                               trip_id: plan["trip_id"] as! String,
                                                               typeReceived: plan["type"] as! String,
                                                               weather: plan["weather"] as! String )
                                
                                  }
                              }
    
                            }
                        
                        }
                        
                    }
                    self.loadData()
                  Helper.app.successAlert(NSLocalizedString("Success", comment: ""),
                                          vc: self)
                }else{
                    
                  Helper.app.failedAlert(NSLocalizedString("Failed", comment: ""),
                                         vc: self)
                    
                }
                
            })
            return
            
         }else  {
            Helper.app.failedAlert(NSLocalizedString("NO INTERNET", comment: ""),  vc: self)
            return
        }
        
        
    }
    func doSync(_ AllMyTripsTemp: Mytrips){
        
        if(Helper.app.isInternetAvailable()){
           
        var myTripsArray = [String: Any]()
        var tripdetails: Dictionary = [String: Any]()
   
        tripdetails["name"] = AllMyTripsTemp.name
        tripdetails["destination"] = AllMyTripsTemp.destination
        tripdetails["departuredate"] = AllMyTripsTemp.departuredate
        tripdetails["finisheddate"] = AllMyTripsTemp.finisheddate
        tripdetails["md5_pass"] = AllMyTripsTemp.md5_pass
        tripdetails["unique_id"] = AllMyTripsTemp.unique_id
        tripdetails["id"] = AllMyTripsTemp.id
        tripdetails["created"] = NSDate().timeIntervalSince1970
            
        var myPlansArray = [[String: Any]]()
        
        let text = ""
        let trip_idbe = AllMyTripsTemp.unique_id
        let MyplansRequest:NSFetchRequest<Myplans> = Myplans.fetchRequest()
        
        MyplansRequest.predicate = NSPredicate(format:"%K == %@", "trip_id", AllMyTripsTemp.unique_id!)
        
        do {
            let fetchedPlans = try moc.fetch(MyplansRequest)
            for plan in fetchedPlans {
                
                let planType = plan.type
                let plans_ids = plan.plans_id
                var myPlansFormsArray = [[String: Any]]()
                
                if(planType == "Checklist" ){
                    
                    let travelPlanOneRequest:NSFetchRequest<Planstypechecklist> = Planstypechecklist.fetchRequest()
                    let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
                    travelPlanOneRequest.sortDescriptors = [sortDescriptor]
                    
                    travelPlanOneRequest.predicate = NSPredicate(format:"%K == %@", "trip_id", AllMyTripsTemp.unique_id!)
                    travelPlanOneRequest.predicate = NSPredicate(format:"%K == %@", "plans_id", plans_ids!)

                    do {
                        let fetchedPlans = try moc.fetch(travelPlanOneRequest)
                        for plan in fetchedPlans {
                            
                            let planStrings = ["type": "\( plan.type ??  text)",
                                "trip_id": "\(plan.trip_id ??  text)",
                                "qty": "\( plan.qty ??  text)",
                                "date": "\(plan.date ??  text)",
                                "plans_id": "\( plan.plans_id ??  text)",
                                "isdone": "\( plan.isdone)" ,
                                "item_id": "\( plan.item_id ??  text)",
                                "task": "\( plan.task ??  text)" ,
                                "title": "\( plan.title ??  text)" ,
                                "destination": "\(plan.destination ??  text)"]
                            
                            myPlansFormsArray.append(planStrings)
                        }
                    } catch {
                        Helper.app.failedAlert(NSLocalizedString("Failed", comment: ""),
                                               vc: self)
                    }
                }else if(planType == "Accommadation" || planType == "Transportion" ){

                    let travelPlanTwoRequest:NSFetchRequest<Planstypeother> = Planstypeother.fetchRequest()
                    let sortDescriptor2 = NSSortDescriptor(key: "arrivalDate", ascending: false)
                    travelPlanTwoRequest.sortDescriptors = [sortDescriptor2]
                    travelPlanTwoRequest.predicate = NSPredicate(format:"%K == %@", "trip_id", AllMyTripsTemp.unique_id!)
                    travelPlanTwoRequest.predicate = NSPredicate(format:"%K == %@", "plan_id", plans_ids!)
                    
                    do {
                        let fetchedPlans = try moc.fetch(travelPlanTwoRequest)
                        for plan in fetchedPlans {
                            let planString = ["type": "\( plan.type ??  text)",
                                "trip_id": "\(plan.trip_id ??  text)",
                                "departureDate": "\(plan.departureDate ??  text)",
                                "arrivalDate": "\( plan.arrivalDate ??  text)" ,
                                "arrivalAddress": "\(plan.arrivalAddress ??  text)",
                                "accommodationName": "\(plan.accommodationName ??  text)",
                                "plan_id": "\( plan.plan_id ??  text)" ,
                                "numberOfNights": "\( plan.numberOfNights ??  text)" ,
                                "reservationNo": "\(plan.reservationNo ??  text)",
                                "departureAddress": "\( plan.departureAddress ??  text)" ,
                                "transport": "\(plan.transport ??  text)",
                                "details": "\(plan.details ??  text)"
                            ]
                            
                            myPlansFormsArray.append(planString)
                        }
                    } catch {
                        Helper.app.failedAlert(NSLocalizedString("Failed", comment: ""),
                                               vc: self)
                    }
 
                }else if(planType == "Itinerary Notes" || planType == "Daily Itinerary" ){
                    
                    let travelPlanThreeRequest:NSFetchRequest<Planstypeitinerary> = Planstypeitinerary.fetchRequest()
                    
                    travelPlanThreeRequest.predicate = NSPredicate(format:"%K == %@", "trip_id", AllMyTripsTemp.unique_id!)
                    travelPlanThreeRequest.predicate = NSPredicate(format:"%K == %@", "plan_id", plans_ids!)
                    let sortDescriptor3 = NSSortDescriptor(key: "date", ascending: false)
                    travelPlanThreeRequest.sortDescriptors = [sortDescriptor3]
                    do {
                        let fetchedPlans = try moc.fetch(travelPlanThreeRequest)
                        for plan in fetchedPlans {
                            let planString = ["type": "\( plan.type ??  text)",
                                "trip_id": "\(plan.trip_id ??  text)",
                                "category": "\(plan.category ??  text)",
                                "date": "\(plan.date ??  text)",
                                "plan_id": "\( plan.plan_id ??  text)",
                                "item_id": "\( plan.item_id ??  text)" ,
                                "destination": "\(plan.destination ??  text)",
                                "notes": "\(plan.notes ??  text)",
                                "title": "\(plan.title ??  text)",
                                "weather": "\(plan.weather ??  text)"]
                            
                            myPlansFormsArray.append(planString)
                        }
                    } catch {
                        Helper.app.failedAlert(NSLocalizedString("Failed", comment: ""),
                            vc: self)
                    }
                }
                
                let planString = ["type": "\( plan.type ??  text)",
                    "trip_id": "\(plan.trip_id ??  text)",
                    "date": "\(plan.date ??  text)",
                    "plans_id": "\( plan.plans_id ??  text)" ,
                    "rank": "\( plan.rank )" ,
                    "destination": "\(plan.destination ??  text)",
                    "plan": myPlansFormsArray] as [String : Any]
               myPlansArray.append(planString)
            }
        } catch {
            Helper.app.failedAlert(NSLocalizedString("Failed", comment: ""),
                                   vc: self)
        }

        tripdetails["plans"] = myPlansArray

        myTripsArray = [trip_idbe : tripdetails] as! [String : Any]
        ref = Database.database().reference();
        
        ref.child("trip").child(AllMyTripsTemp.unique_id!).setValue(myTripsArray)
            
            
        } else {
            Helper.app.successAlert(NSLocalizedString("NO INTERNET", comment: ""),  vc: self)
        }
    }
    
    
    func addTrip(name: String, destination: String,  departuredate: String,  finishDate: String, password: String, unique_id: String, id: String)  {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Mytrips",
                                       in: managedContext)!
        
        let trip = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        trip.setValue(name, forKeyPath: "name")
        trip.setValue(destination, forKeyPath: "destination")
        trip.setValue(departuredate, forKeyPath: "departuredate")
        trip.setValue(finishDate, forKeyPath: "finisheddate")
        trip.setValue(password, forKeyPath: "md5_pass")
        trip.setValue(unique_id, forKeyPath: "unique_id")
        trip.setValue(id, forKeyPath: "id")
        
        do {
            try managedContext.save()
            Helper.app.failedAlert(NSLocalizedString("Successfully Created Trip", comment: ""), vc: self)
            
        } catch _ as NSError {
          
            Helper.app.failedAlert(NSLocalizedString("Failed To Create Trip", comment: ""), vc: self)
        }
    }
    
    func checkIfTripExists(_ trip_id : String) -> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Mytrips")
        
        entity.predicate = NSPredicate(format: "unique_id == %@", trip_id)
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count == 1 {
            return true
        }else{
        
            return false
        }
        
    }
    
    @IBAction func sendMail(_ sender: Any) {

        // define email address
        let address = NSLocalizedString( "jamescostel@gmail.com", comment: "") 
     
        // create mail subject
        let subject = NSLocalizedString("subject", comment: "")
        
        let url = URL(string: "mailto:?to=\(address)&subject=\(subject)")
        if UIApplication.shared.canOpenURL(url!) {
           
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
