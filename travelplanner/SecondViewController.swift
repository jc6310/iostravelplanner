//
//  SecondViewController.swift
//  travelplanner
//
//  Created by James Costello on 01/12/2018.
//  Copyright © 2018 James Costello. All rights reserved.
//
import UIKit
import CoreData

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    
    var detailToSend = [Myplans]()
    var detailReceived = [Mytrips]()
    var typeToSend = ""
    var trip_id = ""
    let cellReuseIdentifier = "cell"
    var AllMyPlans = [Myplans]()
    var moc:NSManagedObjectContext!
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = detailReceived.first?.value(forKeyPath: "name") as? String
        trip_id = (detailReceived.first?.value(forKeyPath: "unique_id") as? String)!
    
        moc = appDel.persistentContainer.viewContext
        
        tableview.dataSource = self
        tableview.delegate = self
        
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#0A0D20")
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }

    func loadData(){

        let MyplansRequest:NSFetchRequest<Myplans> = Myplans.fetchRequest()
        
        let sortDescriptor = NSSortDescriptor(key: "plans_id", ascending: false)
        MyplansRequest.sortDescriptors = [sortDescriptor]
        MyplansRequest.predicate = NSPredicate(format:"%K == %@", "trip_id", trip_id)

        do {
            try AllMyPlans = moc.fetch(MyplansRequest)
        } catch {
            Helper.app.failedAlert(NSLocalizedString("FailedToLoadData",
                                                     comment: ""),
                                   vc: self)
        }
        self.tableview.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int  {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return AllMyPlans.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        height = 70
        
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let listMyTrips = AllMyPlans[indexPath.row]
        
        let cell:SecondViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! SecondViewTableViewCell
        
        cell.nameLabel.text =
            listMyTrips.value(forKeyPath: "destination") as? String
        
        let planType = listMyTrips.value(forKeyPath: "type") as? String
        
        if(planType=="Checklist" ){
             cell.typeLabel.text = NSLocalizedString("Checklist",comment: "")
        }else if(planType=="Itinerary Notes"){
             cell.typeLabel.text = NSLocalizedString("ItineraryNotes",comment: "")
        }else if(planType=="Daily Itinerary"){
            cell.typeLabel.text = NSLocalizedString("DailyItinerary",comment: "")
        }else if(planType=="Transportion"){
             cell.typeLabel.text = NSLocalizedString("Transportion",comment: "")
        }else if(planType=="Accommadation"){
            cell.typeLabel.text = NSLocalizedString("Accommadation", comment: "")
        }

        cell.layer.borderWidth = CGFloat(3)
        cell.layer.borderColor = tableView.backgroundColor?.cgColor
        
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let type = "\(self.AllMyPlans[indexPath.row].type  ?? "")"
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        if(type=="Transportion" ){
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "transportForm") as! TransportViewController
            
            nextViewController.detailReceived = [self.AllMyPlans[indexPath.row]]
            nextViewController.isEditReceived = "view"
            nextViewController.tripDetail = detailReceived
            nextViewController.trip_id = self.trip_id
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        }
        
        if(type=="Accommadation"){
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "infoForms") as! FormsViewController
            
            nextViewController.detailReceived = [self.AllMyPlans[indexPath.row]]
            nextViewController.isEditReceived = "view"
            nextViewController.tripDetail = detailReceived
            nextViewController.trip_id = self.trip_id
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        }
  
        if(type=="Itinerary Notes" || type=="Daily Itinerary"){
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "itineraryforms") as! FormsTwoViewController
            
            nextViewController.detailReceived = [self.AllMyPlans[indexPath.row]]
            nextViewController.isEditReceived = "view"
            nextViewController.tripDetail = detailReceived
            nextViewController.trip_id = self.trip_id
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        }
        
        if(type=="Checklist" ){
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "formsplannerone") as! FormsOneViewController
            
            nextViewController.detailReceived = [self.AllMyPlans[indexPath.row]]
            nextViewController.isEditReceived = "view"
            nextViewController.tripDetail = detailReceived
            nextViewController.trip_id = self.trip_id
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        }
    }
 
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let editAction = UITableViewRowAction(style: .normal,
                                              title: NSLocalizedString("Edit",
                                                                       comment: "")
        ) { (rowAction, indexPath) in
         
            self.detailToSend = [self.AllMyPlans[indexPath.row]]
      
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let type = self.AllMyPlans[indexPath.row].type
            
            if(type=="Transportion" ){
                let infoViewController = storyBoard.instantiateViewController(withIdentifier: "transportForm") as! TransportViewController
                
                infoViewController.detailReceived = self.detailToSend
                infoViewController.trip_id = self.trip_id
                infoViewController.tripDetail = self.detailReceived
                
                let navigationController = UINavigationController(rootViewController: infoViewController)
                
                self.present(navigationController, animated:true, completion:nil)
                
            } else if(type=="Accommadation" ){
                let infoViewController = storyBoard.instantiateViewController(withIdentifier: "infoForms") as! FormsViewController
                
                infoViewController.detailReceived = self.detailToSend
                infoViewController.trip_id = self.trip_id
                infoViewController.tripDetail = self.detailReceived
                
                let navigationController = UINavigationController(rootViewController: infoViewController)
                
                self.present(navigationController, animated:true, completion:nil)
            
            }else if(type=="Daily Itinerary" || type=="Itinerary Notes"){
                
                let itineraryViewController = storyBoard.instantiateViewController(withIdentifier: "itineraryforms") as! FormsTwoViewController
                
                itineraryViewController.detailReceived = self.detailToSend
                itineraryViewController.trip_id = self.trip_id
                itineraryViewController.tripDetail = self.detailReceived
                
                
                let navigationController = UINavigationController(rootViewController: itineraryViewController)
                
                self.present(navigationController, animated:true, completion:nil)
            
                
            }else if( type=="Checklist" ){
                let checklistsViewController = storyBoard.instantiateViewController(withIdentifier: "formsplannerone") as! FormsOneViewController
                
                checklistsViewController.detailReceived = self.detailToSend
                checklistsViewController.trip_id = self.trip_id
                checklistsViewController.tripDetail = self.detailReceived
                
                let navigationController = UINavigationController(rootViewController: checklistsViewController)
                
                self.present(navigationController, animated:true, completion:nil)

            }else{
                Helper.app.failedAlert(NSLocalizedString("ErrorNothingMatches",
                                                         comment: ""),
                                       vc: self)
            }
        }
        editAction.backgroundColor = .blue
        
        let deleteAction = UITableViewRowAction(style: .normal,
                                                title: NSLocalizedString("Delete",
                                                                         comment: "")
        ) { (rowAction, indexPath) in
            
            let alert = UIAlertController(title:
                NSLocalizedString("AreYouSureYouWantToDelete",
                                  comment: ""),
                                          message:
                NSLocalizedString("ThisIsPermanent",
                                  comment: ""),
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title:
                NSLocalizedString("Yes",
                                  comment: ""),
                                          style: .default,
                                          handler: { action in self.deletePlans(
                                            self.AllMyPlans[indexPath.row])
            } ))
            alert.addAction(UIAlertAction(title:
                NSLocalizedString("No",
                                  comment: ""),
                                          style: .cancel,
                                          handler: nil))
            
            self.present(alert, animated: true)
        }
        deleteAction.backgroundColor = .red
  
        return [deleteAction,editAction]
    }
    
    @IBAction func moreOptions(_ sender: Any) {
        
        let alertController = UIAlertController(title:
            NSLocalizedString("SelectPlan",
                              comment: ""),
                                                message: nil,
                                                preferredStyle: .alert)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
     
        let transportationForm = UIAlertAction(title:
            NSLocalizedString("Transportion",
                              comment: ""),
                                               style: .default,
                                               handler:  { action in
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "transportForm") as! TransportViewController
            
            nextViewController.typeReceived = "Transportion"
            nextViewController.trip_id = self.trip_id
            nextViewController.tripDetail = self.detailReceived
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
       })
        
        let accommadationForm = UIAlertAction(title:
            NSLocalizedString("Accommadation",
                              comment: ""),
                                              style: .default,
                                              handler:{ action in
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "infoForms") as! FormsViewController
            
            nextViewController.typeReceived = "Accommadation"
            nextViewController.trip_id = self.trip_id
            nextViewController.tripDetail = self.detailReceived
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        })

        let dailyItineraryForm = UIAlertAction(title:
            NSLocalizedString("DailyItinerary",
                              comment: ""),
                                               style: .default,
                                               handler: { action in
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "itineraryforms") as! FormsTwoViewController
            
            nextViewController.typeReceived = "Daily Itinerary"
            nextViewController.trip_id = self.trip_id
            nextViewController.tripDetail = self.detailReceived
            
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        })
        
        let ItineraryForm = UIAlertAction(title:
            NSLocalizedString("ItineraryNotes",
                              comment: ""),
                                          style: .default, handler: { action in
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "itineraryforms") as! FormsTwoViewController
            
            nextViewController.typeReceived = "Itinerary Notes"
            nextViewController.trip_id = self.trip_id
            nextViewController.tripDetail = self.detailReceived
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
            
            self.present(navigationController, animated:true, completion:nil)
        })
        
        let checklistsForm = UIAlertAction(title:
            NSLocalizedString("Checklist",
                              comment: ""),
                                           style: .default,
                                           handler: { action in
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "formsplannerone") as! FormsOneViewController
            
            nextViewController.typeReceived = "Checklist"
            nextViewController.trip_id = self.trip_id
            nextViewController.tripDetail = self.detailReceived
            
            let navigationController = UINavigationController(rootViewController: nextViewController)
         
            self.present(navigationController, animated:true, completion:nil)
        })
       
        let cancelAction = UIAlertAction(title:
            NSLocalizedString("Cancel",
                              comment: ""),
                                         style: .cancel,
                                         handler: {(alert: UIAlertAction!) in print(
                                            NSLocalizedString("Cancel",  comment: ""))
        })
        
        alertController.addAction(transportationForm)
        alertController.addAction(accommadationForm)
        alertController.addAction(dailyItineraryForm)
        alertController.addAction(ItineraryForm)
        alertController.addAction(checklistsForm)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion:{})
    }
    
    func deletePlans(_ AllMyPlans: Myplans){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
          let trip_id =  (AllMyPlans.trip_id) ?? ""
          let plan_type =  (AllMyPlans.type) ?? ""
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(AllMyPlans as NSManagedObject)
        
        do {
            try managedContext.save()
            self.loadData()
            
            if(plan_type=="Itinerary Notes" || plan_type=="Daily Itinerary"){
                 _ = DbHelper.app.deleteItinerary(trip_id: trip_id)
            }else if(plan_type=="Checklist"){
                 _ = DbHelper.app.deleteChecklist(trip_id: trip_id)
            }else if(plan_type=="Transportion" || plan_type=="Accommadation"){
                   _ = DbHelper.app.deleteOthers(trip_id: trip_id)
            }
        } catch _ {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
