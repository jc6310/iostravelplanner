//
//  Helper.swift
//  travelplanner
//
//  Created by James Costello on 14/01/2019.
//  Copyright © 2019 James Costello. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class Helper {
    
    static var app: Helper = {
        return Helper()
    }()
    
    func failedAlert(_ msg: String, vc: UIViewController) {
        
        let alertController = UIAlertController(title: NSLocalizedString("Failed",  comment: "Helper Class"), message:
            msg , preferredStyle: UIAlertController.Style.alert)
        
        let imgTitle = UIImage(named:"icons8-error-filled-50")
        let imgViewTitle = UIImageView(frame: CGRect(x: 10, y: 20, width: 45, height: 45))
        imgViewTitle.image = imgTitle
        imgViewTitle.image = imgViewTitle.image!.withRenderingMode(.alwaysTemplate)
        imgViewTitle.tintColor = UIColor(red: 247/255, green: 0/255, blue: 0/255, alpha: 1.0)
        alertController.view.addSubview(imgViewTitle)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK",  comment: "Helper Class"), style: UIAlertAction.Style.default,handler: nil))
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func successAlert(_ msg: String, vc: UIViewController)  {
        
        let alertController = UIAlertController(title: NSLocalizedString("Success",  comment: "Helper Class"), message:
            msg , preferredStyle: .alert)

        let imgTitle = UIImage(named:"icons8-good-quality-filled-50")
        let imgViewTitle = UIImageView(frame: CGRect(x: 10, y: 20, width: 45, height: 45))
        imgViewTitle.image = imgTitle
        imgViewTitle.image = imgViewTitle.image!.withRenderingMode(.alwaysTemplate)
        imgViewTitle.tintColor = UIColor(red: 0/255, green: 204/255, blue: 3/255, alpha: 1.0)
        alertController.view.addSubview(imgViewTitle)

        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK",  comment: "Helper Class"), style: UIAlertAction.Style.default,handler: nil))
        
        vc.present(alertController, animated: true, completion: nil)
    }

    func warningAlert(_  title: String, msg: String, vc: UIViewController)  {
        
        let alertController = UIAlertController(title: title, message:
            msg , preferredStyle: .alert)
        
        let imgTitle = UIImage(named:"icons8-error-filled-50")
        let imgViewTitle = UIImageView(frame: CGRect(x: 10, y: 20, width: 45, height: 45))
        imgViewTitle.image = imgTitle
        imgViewTitle.image = imgViewTitle.image!.withRenderingMode(.alwaysTemplate)
        imgViewTitle.tintColor = UIColor(red: 242/255, green: 121/255, blue: 0/255, alpha: 1.0)
        alertController.view.addSubview(imgViewTitle)
       
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK",  comment: "Helper Class"), style: UIAlertAction.Style.default,handler: nil))
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func randomNumbersOnly(_ n: Int) -> String {
        
        let a = "1234567890"
        var s = ""
        
        for _ in 0..<n{
            
            let r = Int(arc4random_uniform(UInt32(a.count)))
            s += String(a[a.index(a.startIndex, offsetBy: r)])
        }
        
        return s
    }

    func random(_ n: Int) -> String {
        
        let a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        var s = ""
        
        for _ in 0..<n{
            
            let r = Int(arc4random_uniform(UInt32(a.count)))
            s += String(a[a.index(a.startIndex, offsetBy: r)])
        }
        
        return s
    }
    
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
