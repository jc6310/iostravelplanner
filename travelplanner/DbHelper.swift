//
//  DbHelper.swift
//  travelplanner
//
//  Created by James Costello on 04/03/2019.
//  Copyright © 2019 James Costello. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DbHelper {
    
    static var app: DbHelper = {
        return DbHelper()
    }()
    
    var moc:NSManagedObjectContext!
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func addTrip(name: String, destination: String,  departuredate: String,  finishDate: String, password: String, unique_id: String , id: String, rank: Double )  -> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Mytrips",
                                       in: managedContext)!
        
        let trip = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
  
        trip.setValue(name, forKeyPath: "name")
        trip.setValue(destination, forKeyPath: "destination")
        trip.setValue(departuredate, forKeyPath: "departuredate")
        trip.setValue(finishDate, forKeyPath: "finisheddate")
        trip.setValue(password, forKeyPath: "md5_pass")
        trip.setValue(unique_id, forKeyPath: "unique_id")
        trip.setValue(id, forKeyPath: "id")
        trip.setValue(rank, forKeyPath: "rank")
        
        do {
            try managedContext.save()
            return true
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
                 return false
        }
    }
    
    func addPlan(destination: String, date: String,  plans_id: String,  trip_id: String, typeReceived: String, rank: Double)  -> Bool {
    
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
              return false
        }
            
        let managedContext = appDelegate.persistentContainer.viewContext
            
        let entity =
                NSEntityDescription.entity(forEntityName: "Myplans",
                                           in: managedContext)!
            
        let trip = NSManagedObject(entity: entity,
                                       insertInto: managedContext)

            trip.setValue(destination, forKeyPath: "destination")
            trip.setValue(rank, forKeyPath: "rank")
            trip.setValue(date, forKeyPath: "date")
            trip.setValue(plans_id, forKeyPath: "plans_id")
            trip.setValue(trip_id, forKeyPath: "trip_id")
            trip.setValue(typeReceived, forKeyPath: "type")
            
        do {
              try managedContext.save()
              return true
         } catch _ as NSError {
                return false
         }
    }
    
    func addChecklist(item_id: String, date: String,  destination: String, qty: String, task : String, plans_id: String, title: String, trip_id: String, typeReceived: String, isDone: Bool )  -> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Planstypechecklist",
                                       in: managedContext)!
        
        let trip = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        trip.setValue(item_id, forKeyPath: "item_id")
        trip.setValue(title, forKeyPath: "title")
        trip.setValue(date, forKeyPath: "date")
        trip.setValue(destination, forKeyPath: "destination")
        trip.setValue(qty, forKeyPath: "qty")
        trip.setValue(plans_id, forKeyPath: "plans_id")
        trip.setValue(task, forKeyPath: "task")
        trip.setValue(trip_id, forKeyPath: "trip_id")
        trip.setValue(typeReceived, forKeyPath: "type")
        trip.setValue(isDone, forKeyPath: "isdone")
        
        do {
            try managedContext.save()
            return true
        } catch _ as NSError {
            return false
        }
    }
    
    func addTransportAcco(accommodationName: String, arrivalDate: String,  arrivalAddress: String,  departureAddress: String, departureDate: String, numberOfNights: String, plan_id: String, reservationNo: String, transport: String, trip_id: String, type: String, details: String )  -> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity =  NSEntityDescription.entity(forEntityName: "Planstypeother", in: managedContext)!
        
        let trip = NSManagedObject(entity: entity, insertInto: managedContext)
        
        trip.setValue(accommodationName, forKeyPath: "accommodationName")
        trip.setValue(arrivalDate, forKeyPath: "arrivalDate")
        trip.setValue(arrivalAddress, forKeyPath: "arrivalAddress")
        trip.setValue(departureAddress, forKeyPath: "departureAddress")
        trip.setValue(departureDate, forKeyPath: "departureDate")
        trip.setValue(details, forKeyPath: "details")
        trip.setValue(numberOfNights, forKeyPath: "numberOfNights")
        trip.setValue(plan_id, forKeyPath: "plan_id")
        trip.setValue(reservationNo, forKeyPath: "reservationNo")
        trip.setValue(transport, forKeyPath: "transport")
        trip.setValue(trip_id, forKeyPath: "trip_id")
        trip.setValue(type, forKeyPath: "type")
        trip.setValue(details, forKeyPath: "details")
        
        do {
            try managedContext.save()
            return true
        } catch _ as NSError {
            return false
        }

    }
    
    func addItinerary(item_id: String, date: String, destination: String, category: String, title: String,
                          plans_id: String, text: String, trip_id:  String, typeReceived: String, weather: String  )  {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Planstypeitinerary",
                                       in: managedContext)!
        
        let trip = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        trip.setValue(item_id, forKeyPath: "item_id")
        trip.setValue(date, forKeyPath: "date")
        trip.setValue(title, forKeyPath: "title")
        trip.setValue(destination, forKeyPath: "destination")
        trip.setValue(weather, forKeyPath: "weather")
        trip.setValue(category, forKeyPath: "category")
        trip.setValue(plans_id, forKeyPath: "plan_id")
        trip.setValue(text, forKeyPath: "notes")
        trip.setValue(trip_id, forKeyPath: "trip_id")
        trip.setValue(typeReceived, forKeyPath: "type")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    func updateTrips(nameText: String, destinationText: String,  departureDate: String, finishDate: String, unique_id: String) -> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Mytrips")
        
        entity.predicate = NSPredicate(format:"unique_id = %@", unique_id)
        
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count == 1 {
            
            let dic = result![0]
            dic.setValue(destinationText, forKeyPath: "destination")
            dic.setValue(nameText, forKeyPath: "name")
            dic.setValue(departureDate, forKeyPath: "departuredate")
            dic.setValue(finishDate, forKeyPath: "finisheddate")
            
            do {
                try managedContext.save()
                 return true
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
                return false
            }
        }
      return false
    }
    
    func updatePlan(destination: String, date: String,  plans_id: String, rank: Double) -> Bool {
    
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Myplans")
        
        entity.predicate = NSPredicate(format:"plans_id = %@", plans_id)
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count == 1 {
            
            let dic = result![0]
            dic.setValue(destination, forKeyPath: "destination")
            dic.setValue(date, forKeyPath: "date")
            dic.setValue(rank, forKeyPath: "rank")
            
            
            do {
                try managedContext.save()
                return true
            } catch _ as NSError {
                return false
            }
        }
       return false
    }
    
    func updateTransportAcco(accommodationName: String, arrivalDate: String,  arrivalAddress: String,  departureAddress: String, departureDate: String, numberOfNights: String, plan_id: String, reservationNo: String, transport: String, trip_id: String, type: String, details: String ) -> Bool {
        
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
            return false
      }
    
      let managedContext = appDelegate.persistentContainer.viewContext
    
      let entity =  NSFetchRequest<NSManagedObject>(entityName: "Planstypeother")
    
      entity.predicate = NSPredicate(format:"plan_id = %@", plan_id)
 
      let result = try? managedContext.fetch(entity)
    
      if result?.count == 1 {
        
        let dic = result![0]
     
        dic.setValue(accommodationName, forKeyPath: "accommodationName")
        dic.setValue(arrivalDate, forKeyPath: "arrivalDate")
        dic.setValue(arrivalAddress, forKeyPath: "arrivalAddress")
        dic.setValue(departureAddress, forKeyPath: "departureAddress")
        dic.setValue(departureDate, forKeyPath: "departureDate")
        dic.setValue(details, forKeyPath: "details")
        dic.setValue(numberOfNights, forKeyPath: "numberOfNights")
        dic.setValue(plan_id, forKeyPath: "plan_id")
        dic.setValue(reservationNo, forKeyPath: "reservationNo")
        dic.setValue(transport, forKeyPath: "transport")
        dic.setValue(trip_id, forKeyPath: "trip_id")
        dic.setValue(type, forKeyPath: "type")
        dic.setValue(details, forKeyPath: "details")

        do {
            try managedContext.save()
            return true
        } catch _ as NSError {
            return false
        }
     }
     return false
   }
   
    func deletetrip(trip_id: String, _ AllMyTripsTemp: Mytrips) -> Bool {

        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
    
        let managedContext = appDelegate.persistentContainer.viewContext
           managedContext.delete(AllMyTripsTemp as NSManagedObject)
        
        do {
            try managedContext.save()
            _ = deletePlan(trip_id: trip_id)
            return true
        } catch _ as NSError {
            return false
        }
    }
    
    func deletePlan(trip_id: String)-> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =  NSFetchRequest<NSManagedObject>(entityName: "Myplans")
        
        entity.predicate = NSPredicate(format:"trip_id = %@", trip_id)
        
        do {
            let fetchedResults =  try managedContext.fetch(entity as! NSFetchRequest<NSFetchRequestResult>) as? [NSManagedObject]
            
                for entity in fetchedResults! {
                    managedContext.delete(entity)
                }
             _ = deleteItinerary(trip_id: trip_id)
             _ = deleteChecklist(trip_id: trip_id)
             _ = deleteOthers(trip_id: trip_id)
            
              return true
        } catch {
              print("Could not delete")
             return false
        }
   }
   
    func deleteItinerary(trip_id: String)-> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =  NSFetchRequest<NSManagedObject>(entityName: "Planstypeitinerary")
        
        entity.predicate = NSPredicate(format:"trip_id = %@", trip_id)
        
        do {
            let fetchedResults =  try managedContext.fetch(entity as! NSFetchRequest<NSFetchRequestResult>) as? [NSManagedObject]
            
            for entity in fetchedResults! {
                managedContext.delete(entity)
            }
            return true
        } catch {
            print("Could not delete")
            return false
        }
    }
    
    func deleteChecklist(trip_id: String)-> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =  NSFetchRequest<NSManagedObject>(entityName: "Planstypechecklist")
        
        entity.predicate = NSPredicate(format:"trip_id = %@", trip_id)
        
        do {
            let fetchedResults =  try managedContext.fetch(entity as! NSFetchRequest<NSFetchRequestResult>) as? [NSManagedObject]
            
            for entity in fetchedResults! {
                managedContext.delete(entity)
            }
            return true
        } catch {
            print("Could not delete")
            return false
        }
    }
    
    func deleteOthers(trip_id: String)-> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =  NSFetchRequest<NSManagedObject>(entityName: "Planstypeother")
        
        entity.predicate = NSPredicate(format:"trip_id = %@", trip_id)
        
        do {
            let fetchedResults =  try managedContext.fetch(entity as! NSFetchRequest<NSFetchRequestResult>) as? [NSManagedObject]
            
            for entity in fetchedResults! {
                managedContext.delete(entity)
            }
            return true
        } catch {
            print("Could not delete")
            return false
        }
    }
    
    
   func checkIfTripExists(_ trip_id : String) -> Bool {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSFetchRequest<NSManagedObject>(entityName: "Mytrips")
        
        entity.predicate = NSPredicate(format: "unique_id == %@", trip_id)
        
        let result = try? managedContext.fetch(entity)
        
        if result?.count == 1 {
            print("does  exist")
            return true
        }else{
            print("does not exist")
            return false
        }
        
    }

    func convertDateToTimeStamp(_ date : Date)-> Double {
         return date.timeIntervalSince1970
    }
}
